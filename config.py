
class trainCfg():
    def __init__(self, BATCH_SIZE, EPOCH, LR, WEIGHT_DECAY, model_type_id, set_id, train_test_eval_index_id, train_and_test_epoch, supervise_ratio, test_only_EDA, use_dataset_percent):
        # model name
        self.model_types = ["cnn", "rnn", "bert"]
        ###########
        ###########
        self.model_type_id = model_type_id
        ###########
        ###########


        ###########
        ###########
        self.test_only_EDA = test_only_EDA
        ###########
        ###########

        # dataset folder
        #                  0      1     2        3     4
        self.datasets = ['cr', 'sst', 'subj', 'trec', 'mr']
        # number of words for input
        self.input_size_list = [40, 40, 40, 25, 30]

        ###########
        ###########
        self.set_id = set_id
        ###########
        ###########

        # number of output classes
        self.num_classes_list = [2, 2, 2, 6, 2]

        #wordMixup and sentMixup

        # number of augmentations

        # alpha values we care about
        #               sr    ri   rs,  rd
        self.alphas = [0.2, 0.2, 0.2, 0.2]

        # aug params
        self.num_aug = 1

        #
        self.use_dataset_percent = use_dataset_percent



        # word2vec dictionary # used in the data process, not used in the training stage.
        self.huge_word2vec = '../../dataset/glove.840B.300d.txt'
        # don't want to load the huge pickle every time, so just save the words that are actually used into a smaller dictionary
        self.word2vec_len = 300

        # train dict and txt
        self.word2vec_weight_path = 'data/{}/train_{}_word2vec.pkl'.format(
            self.datasets[self.set_id], self.datasets[self.set_id])
        self.train_txt = 'data/{}/train_orig.txt'.format(
            self.datasets[self.set_id])

        # eval dict and txt
        # word2vec_weight_path = 'data/sst/train_sst_word2vec.pkl' # same as the train dict
        self.test_txt = 'data/{}/test.txt'.format(self.datasets[self.set_id])

        # train params
        self.BATCH_SIZE = BATCH_SIZE
        if (self.model_types[self.model_type_id] == 'bert'):
            self.BATCH_SIZE = 80

        self.EPOCH = EPOCH
        if (self.model_type_id == 2):  # Bert small lr
            self.LR = 3e-5
        else:
            self.LR = LR
            
        self.WEIGHT_DECAY = WEIGHT_DECAY # 10e-5
        self.temperature = 0.5
        self.use_cosine_similarity = True

        self.run_models = ['train', 'test', 'eval']
        #########
        #########
        self.train_test_eval_index_id = train_test_eval_index_id
        #########
        #########
        self.train_test_eval_switch = self.run_models[self.train_test_eval_index_id]
        self.supervise_ratio = supervise_ratio  # [0.0, 1.0)

        if (self.train_test_eval_switch == 'train'):  # train the CLR model or CLR+CLS (sim-supervised when supervise_ratio is in the range of [0,1))
            self.use_pretrain_clr = False
            self.use_pretrain_cls = False
            self.eval_switch = False
        elif (self.train_test_eval_switch == 'test'):  # train the CLS model
            self.supervise_ratio = 1.0  # [0.0, 1.0)
            self.use_pretrain_clr = True
            self.use_pretrain_cls = False
            self.eval_switch = False
        else:  # eval the CLS (downstram task)
            self.supervise_ratio = 1.0  # [0.0, 1.0)
            self.use_pretrain_clr = True
            self.use_pretrain_cls = True
            self.eval_switch = True
            self.BATCH_SIZE = 1

        # mode params
        if (self.model_types[self.model_type_id] == 'rnn'):
            self.projection_out_dim = 64
        else:
            self.projection_out_dim = 128
        # mixco model alpha of lambda
        # self.mix_lambda_alpha = 0.0
        self.mix_lambda_alpha = 0.2

        # mixco loss weight (percent)
        self.mixco_loss_weight_gamma = 0.2


        # model/log save and load dir
        self.model_checkpoints_folder = 'log/model/'
        self.train_log_dir = 'log/train/'
        ###################
        ###################
        self.train_and_test_epoch = train_and_test_epoch
        ###################
        ###################
        if (self.train_test_eval_switch == 'test'):
            if (supervise_ratio > 0):
                self.model_path = 'log/model/{}/CLR_CLS{}_{}_e{}_train_{}.dit'.format(
                    self.datasets[self.set_id], supervise_ratio, self.model_types[self.model_type_id], self.train_and_test_epoch, use_dataset_percent)
            else:
                self.model_path = 'log/model/{}/CLR_{}_e{}_train_{}.dit'.format(
                    self.datasets[self.set_id], self.model_types[self.model_type_id], self.train_and_test_epoch, use_dataset_percent)
        elif (self.train_test_eval_switch == 'eval'):
            self.model_path = 'log/model/{}/CLR_CLS{}_{}_e{}_test_{}.dit'.format(
                self.datasets[self.set_id], supervise_ratio, self.model_types[self.model_type_id], self.train_and_test_epoch, use_dataset_percent)  # must with a self.supervise_ratio = 1.0 for eval, but for reading the path, here we use the train and test setting
        else:  # train
            self.model_path = ''

        # bert model path
        self.pretrained_path = '/home/rjx/dataset/models/bert-base-uncased'




def genTrainCfg(BATCH_SIZE, EPOCH, LR, WEIGHT_DECAY, model_type_id, set_id, train_test_eval_index_id, train_and_test_epoch, supervise_ratio, test_only_EDA, use_dataset_percent):
    C = trainCfg(BATCH_SIZE, EPOCH, LR, WEIGHT_DECAY, model_type_id, set_id, train_test_eval_index_id, train_and_test_epoch, supervise_ratio, test_only_EDA, use_dataset_percent)
    return C





