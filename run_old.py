from models.textcnn import *
from config import *
import pickle
import numpy as np
import torch
from data_process import *
from torch.utils.data import DataLoader, TensorDataset


if torch.cuda.is_available():
    device = 'cuda:2'
else:
    device = 'cpu'
device = torch.device(device)



class rnnCfg():
    def __init__(self, word2vec_len, class_num, sentence_length, word2vec_weight):
        self.embed_dim = word2vec_len
        self.class_num = class_num
        self.sentence_length = sentence_length
        self.word2vec_weight = torch.tensor(word2vec_weight).float().to(device)

def datasetCfg():
    def __init__(self, word2id, id2word, train_txt, num_classes):
        """
        docstring
        """
        pass

def runCNN(bs=32):
    # Dim = args.embed_dim ##每个词向量长度
    # Cla = args.class_num ##类别数
    # sentence_length = args.sentence_length
    # word2vec_weight = args.word2vec_weight
    word2vec_weight_path = 'data/sst/train_sst_word2vec.pkl'
    train_txt = 'data/sst/train_orig.txt'
    set_id = 1
    word2id, id2word, word2vec_weight = get_dict_and_weight(word2vec_weight_path, word2vec_len)
    args = rnnCfg(300, num_classes_list[set_id], input_size_list[set_id], word2vec_weight)
    
    x_matrix_train, y_matrix_train = get_x_y(train_txt, num_classes_list[set_id], input_size_list[set_id], id2word, word2id)

    ## get the dl
    x_matrix_train, y_matrix_train = torch.tensor(x_matrix_train), torch.tensor(y_matrix_train)
    dataset_train = TensorDataset(x_matrix_train, y_matrix_train)
    train_data_ld = DataLoader(dataset_train, batch_size=bs, shuffle=False, num_workers=2)


    # 



    # model = textCNN(args)