import torch
import torch.nn as nn
import torch.nn.functional as F
from models.textcnn import textCNN
import numpy as np
from CLR.clr_loss import NTXentLoss
from MIXUP.mix_loss import MixCoLoss
import torch.nn.functional as F


class DaCLR(nn.Module):
    def __init__(self, args):
        super(DaCLR, self).__init__()

        self.model = args.base_model
        # self.class_model = nn.Sequential(
        #     nn.Linear(args.projection_out_dim, args.class_num),
        #     nn.Softmax(dim=-1)
        # )

        self.nt_xent_criterion = NTXentLoss(args)

        self.mix_lambda_alpha = args.mix_lambda_alpha

        self.mixco_criterion = MixCoLoss(args)

        self.mixco_loss_weight_gamma = args.mixco_loss_weight_gamma

    def forward(self, x1, x2):
        mix_lambda = None
        if (self.mix_lambda_alpha > 0):
            mix_lambda = np.random.beta(
                self.mix_lambda_alpha, self.mix_lambda_alpha, x1.shape[0])
            mix_lambda = torch.from_numpy(
                mix_lambda).unsqueeze(1).to(x1.device).float()

        # get the representations and the projections
        if (self.mix_lambda_alpha > 0):
            _, z1, mix_z1, mixed_z1 = self.model(x1, mix_lambda)  # 32*128, 32*32
            _, z2, mix_z2, mixed_z2 = self.model(x2, mix_lambda)
        else:
            _, z1 = self.model(x1)  # 32*128, 32*32
            _, z2 = self.model(x2)

        z1 = F.normalize(z1, dim=1)
        z2 = F.normalize(z2, dim=1)

        loss_clr = self.nt_xent_criterion(z1, z2)

        if (self.mix_lambda_alpha > 0):
            loss_mixco_z1 = self.mixco_criterion(
                z1, mix_z1, mixed_z1, mix_lambda)
            loss_mixco_z2 = self.mixco_criterion(
                z2, mix_z2, mixed_z2, mix_lambda)
            loss_mixco = 0.5*loss_mixco_z1 + 0.5*loss_mixco_z2
            return self.mixco_loss_weight_gamma*loss_mixco + (1-self.mixco_loss_weight_gamma)*loss_clr
        else:
            return loss_clr




