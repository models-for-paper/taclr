from torch.utils.data import Dataset
from dataset.utils import get_only_chars
from sklearn.utils import shuffle
from EDA.eda import *
import numpy as np
import torch
import transformers as trm


class au_dataset(Dataset):
    def __init__(self, args):
        """
        docstring
        """
        train_lines = open(args.train_txt, 'r').readlines()
        shuffle(train_lines)
        self.text = train_lines[:int(args.percent_dataset*len(train_lines))]
        self.len = len(self.text)

        self.word2id = args.word2id
        self.id2word = args.id2word
        
        self.device = args.device
        self.input_size = args.input_size
        self.num_classes = args.num_classes

        self.alpha_sr = args.alpha_sr
        self.alpha_ri = args.alpha_ri
        self.alpha_rs = args.alpha_rs
        self.alpha_rd = args.alpha_rd
        self.num_aug = args.num_aug

    def aug_txt(self, method, txt):
        if (0 == method):
            aug_txt = eda(txt, alpha_sr=self.alpha_sr, alpha_ri=0, alpha_rs=0, p_rd=0, num_aug=self.num_aug)
        elif (1 == method):
            aug_txt = eda(txt, alpha_sr=0, alpha_ri=self.alpha_ri, alpha_rs=0, p_rd=0, num_aug=self.num_aug)
        elif (2 == method):
            aug_txt = eda(txt, alpha_sr=0, alpha_ri=0, alpha_rs=self.alpha_rs, p_rd=0, num_aug=self.num_aug)
        elif (3 == method):
            aug_txt = eda(txt, alpha_sr=0, alpha_ri=0, alpha_rs=0, p_rd=self.alpha_rd, num_aug=self.num_aug)
        else:
            aug_txt = [txt]

        return aug_txt[0]

    def gen_tensor_and_padding_txt(self, sentence):
        #insert x
        words = sentence.split(' ')
        len_w = len(words)
        if (len_w > self.input_size):
            words = words[:self.input_size]
        else:
            pading_len = self.input_size - len_w
            for i in range(pading_len):
                words.append('#padding')
        x_tensor = torch.zeros(self.input_size, dtype=torch.long)
        for j, word in enumerate(words):
            if (word in self.id2word):
                x_tensor[j] = self.word2id[word]
            else:
                x_tensor[j] = self.word2id['#padding']

        return x_tensor, len_w

    def gen_positive_pair(self, line):
        parts = line[:-1].split('\t')
        label = int(parts[0])
        txt= get_only_chars(parts[1])
        txt0_tensor, len_0 = self.gen_tensor_and_padding_txt(txt)
        prop_1 = np.random.randint(0,5)
        prop_2 = np.random.randint(0,5)
        txt1 = self.aug_txt(prop_1, txt)
        txt1_tensor, len_1 = self.gen_tensor_and_padding_txt(txt1)
        txt2 = self.aug_txt(prop_2, txt)
        txt2_tensor, len_2 = self.gen_tensor_and_padding_txt(txt2)

        # y_tensor = torch.zeros(self.num_classes, dtype=torch.float)
        # y_tensor[label] = 1.0
        y_tensor = torch.tensor(label).long()
        #            0                              1                              2                           3                  4      5      6
        return txt0_tensor.to(self.device), txt1_tensor.to(self.device), txt2_tensor.to(self.device), y_tensor.to(self.device), len_0, len_1, len_2

    def __getitem__(self, index):
        txt = self.text[index]
        return self.gen_positive_pair(txt)


    def __len__(self):
        return self.len


class au_dataset_bert(Dataset):
    def __init__(self, args):
        """
        docstring
        """
        train_lines = open(args.train_txt, 'r').readlines()
        shuffle(train_lines)
        self.text = train_lines[:int(args.percent_dataset*len(train_lines))]
        self.len = len(self.text)
        
        self.device = args.device
        self.input_size = args.input_size
        self.num_classes = args.num_classes

        self.alpha_sr = args.alpha_sr
        self.alpha_ri = args.alpha_ri
        self.alpha_rs = args.alpha_rs
        self.alpha_rd = args.alpha_rd
        self.num_aug = args.num_aug

        self.tkl = trm.BertTokenizer.from_pretrained(args.pretrained_path)

    def aug_txt(self, method, txt):
        if (0 == method):
            aug_txt = eda(txt, alpha_sr=self.alpha_sr, alpha_ri=0, alpha_rs=0, p_rd=0, num_aug=self.num_aug)
        elif (1 == method):
            aug_txt = eda(txt, alpha_sr=0, alpha_ri=self.alpha_ri, alpha_rs=0, p_rd=0, num_aug=self.num_aug)
        elif (2 == method):
            aug_txt = eda(txt, alpha_sr=0, alpha_ri=0, alpha_rs=self.alpha_rs, p_rd=0, num_aug=self.num_aug)
        elif (3 == method):
            aug_txt = eda(txt, alpha_sr=0, alpha_ri=0, alpha_rs=0, p_rd=self.alpha_rd, num_aug=self.num_aug)
        else:
            aug_txt = [txt]

        return aug_txt[0]

    def gen_tensor_and_padding_txt(self, sentence):
        #insert x
        words = sentence.split(' ')
        len_w = len(words)
        if (len_w > self.input_size-2):
            words = words[:self.input_size-2]
            words = [self.tkl.cls_token] + words + [self.tkl.sep_token]
        else:
            pading_len = self.input_size-2 - len_w
            words = [self.tkl.cls_token] + words + [self.tkl.sep_token]
            for i in range(pading_len):
                words.append(self.tkl.pad_token)
        x_tensor = torch.tensor(self.tkl.convert_tokens_to_ids(words))        
        return x_tensor, len_w

    def gen_positive_pair(self, line):
        parts = line[:-1].split('\t')
        label = int(parts[0])
        txt= get_only_chars(parts[1])
        txt0_tensor, len_0 = self.gen_tensor_and_padding_txt(txt)
        prop_1 = np.random.randint(0,5)
        prop_2 = np.random.randint(0,5)
        txt1 = self.aug_txt(prop_1, txt)
        txt1_tensor, len_1 = self.gen_tensor_and_padding_txt(txt1)
        txt2 = self.aug_txt(prop_2, txt)
        txt2_tensor, len_2 = self.gen_tensor_and_padding_txt(txt2)

        # y_tensor = torch.zeros(self.num_classes, dtype=torch.float)
        # y_tensor[label] = 1.0
        y_tensor = torch.tensor(label).long()
        #            0                              1                              2                           3                  4      5      6
        return txt0_tensor.to(self.device), txt1_tensor.to(self.device), txt2_tensor.to(self.device), y_tensor.to(self.device), len_0, len_1, len_2

    def __getitem__(self, index):
        txt = self.text[index]
        return self.gen_positive_pair(txt)


    def __len__(self):
        return self.len
