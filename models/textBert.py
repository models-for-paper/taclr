import transformers as trm
import torch.nn as nn
import numpy as np
import torch


class textBert(nn.Module):
    def __init__(self, args):
        super(textBert, self).__init__()
        self.pretrained_path = args.pretrained_path
        cfg = trm.BertConfig.from_pretrained(self.pretrained_path)
        self.bertModelOutputSize = cfg.hidden_size

        if (args.pretrain == True):
            self.encoder = nn.ModuleList([
                trm.BertModel.from_pretrained(self.pretrained_path, return_dict=True),
                nn.Linear(self.bertModelOutputSize, 128)
            ])
        else:
            print("###scratch model###")
            self.encoder = nn.ModuleList([
                trm.BertModel(cfg),
                nn.Linear(self.bertModelOutputSize, 128)
            ])
        
        self.projection = nn.Sequential(
            nn.Linear(128,64),
            nn.ReLU(),
            nn.Linear(64,32)
        )
    
    def forward(self, x, mix_lambda = None):
        out = self.encoder[0](x,return_dict=True)
        h = out.pooler_output
        h = self.encoder[1](h)

        if (mix_lambda is not None):
            mix_h = h[torch.from_numpy(np.random.permutation(h.shape[0]))]
            mixed_h = mix_lambda * h + (1.0-mix_lambda) * mix_h

        z = self.projection(h)

        if (mix_lambda is not None):
            mix_z = self.projection(mix_h)
            mixed_z = self.projection(mixed_h)
            return h, z, mix_z, mixed_z
        return h, z
