import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import torch.nn.functional as F


class clsModel(nn.Module):
    def __init__(self, args):
        super(clsModel, self).__init__()
        self.class_model = nn.Sequential(
            nn.Linear(args.projection_out_dim, args.class_num),
            nn.Softmax(dim=-1)
        )
        self.criterion = nn.CrossEntropyLoss()

    def forward(self, h, y):
        logist = self.class_model(h)
        loss = self.criterion(logist, y)
        return loss, logist
