import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np


batch_size = 2
max_length = 3
hidden_size = 2
n_layers = 1
# 这个RNN由两个全连接层组成，对应的两个hidden_state的维度是2，输入向量维度是1
rnn = nn.RNN(1, hidden_size, n_layers, batch_first=True)

class textRNN(nn.Module):
    def __init__(self, args):
        super(textRNN, self).__init__()
        Dim = args.embed_dim ##每个词向量长度
        if (args.pretrain == True):
            word2vec_weight = args.word2vec_weight
            self.embed = nn.Embedding.from_pretrained(word2vec_weight, padding_idx=0)
        else:
            print("###scratch model###")
            word2vec_weight = args.word2vec_weight
            self.embed = nn.Embedding(word2vec_weight.shape[0], word2vec_weight.shape[1], padding_idx=0)


        self.encoder = nn.ModuleList([
            nn.LSTM(
                input_size=Dim,                #The number of expected features in the input x 
                hidden_size=32,     # rnn hidden unit
                num_layers=2,           # number of rnn layers
                batch_first=True,                 # set batch first
                # dropout=0.5,            #dropout probability
                bidirectional=True
                ),
            nn.Dropout(0.1),
            nn.LSTM(
                input_size=128,                #The number of expected features in the input x 
                hidden_size=64,     # rnn hidden unit
                num_layers=1,           # number of rnn layers
                batch_first=True,                 # set batch first
                # dropout=0.5,            #dropout probability
                bidirectional=False
                ),
            nn.Dropout(0.2),
        ])
        self.projection = nn.Sequential(
            # nn.Linear(64,64),
            nn.ReLU(),
            nn.Linear(64,32)
        )
        # nn.Linear(20, Cla)
        # self.sf = nn.Softmax(dim=-1)

    def forward(self, x, mix_lambda = None):
        h = self.embed(x)
        #
        h_0 = torch.zeros((4, x.shape[0], 32), device=x.device)
        c_0 = torch.zeros((4, x.shape[0], 32), device=x.device)
        #
        h, _ = self.encoder[0](h, (h_0, c_0))
        # h = self.encoder[1](h)
        # h, _ = self.encoder[2](h)
        # h = self.encoder[3](h)
        h = h[:,-1,:]

        if (mix_lambda is not None):
            mix_h = h[torch.from_numpy(np.random.permutation(h.shape[0]))]
            mixed_h = mix_lambda * h + (1.0 - mix_lambda) * mix_h

        z = self.projection(h)

        if (mix_lambda is not None):
            mix_z = self.projection(mix_h)
            mixed_z = self.projection(mixed_h)
            return h, z, mix_z, mixed_z
        return h, z

