import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np


class textCNN(nn.Module):
    def __init__(self, args):
        super(textCNN, self).__init__()
        Dim = args.embed_dim ##每个词向量长度
        sentence_length = args.sentence_length
        if (args.pretrain == True):
            word2vec_weight = args.word2vec_weight
            self.embed = nn.Embedding.from_pretrained(word2vec_weight, padding_idx=0)
        else:
            print("###scratch model###")
            word2vec_weight = args.word2vec_weight
            self.embed = nn.Embedding(word2vec_weight.shape[0], word2vec_weight.shape[1], padding_idx=0)


        self.encoder = nn.Sequential(
            nn.Conv1d(Dim, args.projection_out_dim, 5),
            nn.ReLU(),
            nn.MaxPool1d(sentence_length-5+1)
        )
        self.projection = nn.Sequential(
            nn.Linear(128,64),
            nn.ReLU(),
            nn.Linear(64,32)
        )
        # nn.Linear(20, Cla)
        # self.sf = nn.Softmax(dim=-1)

    def forward(self, x, mix_lambda = None):
        h = self.embed(x)
        h = h.permute(0,2,1)
        h = self.encoder(h)
        h = h.squeeze(-1)

        if (mix_lambda is not None):
            mix_h = h[torch.from_numpy(np.random.permutation(h.shape[0]))]
            mixed_h = mix_lambda * h + (1.0-mix_lambda) * mix_h

        z = self.projection(h)

        if (mix_lambda is not None):
            mix_z = self.projection(mix_h)
            mixed_z = self.projection(mixed_h)
            return h, z, mix_z, mixed_z
        return h, z
