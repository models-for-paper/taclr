from sklearn.utils import shuffle
import numpy as np
import pickle

def get_dict_and_weight(word2vec_weight_path, word2vec_len):
	word2vec_dict = pickle.load(open(word2vec_weight_path, 'rb'))
	word2vec_weight = list(word2vec_dict.values())
	word2vec_weight.insert(0, np.zeros(word2vec_len, np.float32))
	word2id = {}
	id2word = []
	word2id['#padding'] = 0
	id2word.append('#padding')
	for i, word in enumerate(word2vec_dict.keys()):
		word2id[word] = i+1
		id2word.append(word)
	return word2id, id2word, word2vec_weight

def get_x_y(train_txt, num_classes, input_size, id2word, word2id, percent_dataset=1):

	#read in lines
	train_lines = open(train_txt, 'r').readlines()
	shuffle(train_lines)
	train_lines = train_lines[:int(percent_dataset*len(train_lines))]
	num_lines = len(train_lines)

	#initialize x and y matrix
	x_matrix = None
	y_matrix = None

	try:
		x_matrix = np.zeros((num_lines, input_size), np.long)
	except:
		print("Error!", num_lines, input_size)
	y_matrix = np.zeros((num_lines, num_classes))
	
	#insert values
	for i, line in enumerate(train_lines):
		parts = line[:-1].split('\t')
		label = int(parts[0])
		sentence = parts[1]	

		#insert x
		words = sentence.split(' ')
		if (len(words) > input_size):
			words = words[:input_size]
		else:
			pading_len = input_size - len(words)
			for i in range(pading_len):
				words.append('#padding')
		for j, word in enumerate(words):
			if (word in id2word):
				x_matrix[i][j] = word2id[word]
			else:
				x_matrix[i][j] = word2id['#padding']

		#insert y
		y_matrix[i][label] = 1.0
	return x_matrix, y_matrix