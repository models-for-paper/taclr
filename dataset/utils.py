import statistics
from os import write
import re
import os
import numpy as np
import pickle
import chardet
# cleaning up text

def get_label(decimal):
    if decimal >= 0 and decimal <= 0.2:
        return 0
    elif decimal > 0.2 and decimal <= 0.4:
        return 1
    elif decimal > 0.4 and decimal <= 0.6:
        return 2
    elif decimal > 0.6 and decimal <= 0.8:
        return 3
    elif decimal > 0.8 and decimal <= 1:
        return 4
    else:
        return -1


def get_label_binary(decimal):
    if decimal >= 0 and decimal <= 0.4:
        return 0
    elif decimal > 0.6 and decimal <= 1:
        return 1
    else:
        return -1


def get_split(split_num):
    if split_num == 1 or split_num == 3:
        return 'train'
    elif split_num == 2:
        return 'test'

def get_only_chars(line):

    clean_line = ""

    line = line.lower()
    line = line.replace(" 's", " is")
    line = line.replace("-", " ")  # replace hyphens with spaces
    line = line.replace("\t", " ")
    line = line.replace("\n", " ")
    line = line.replace("'", "")

    for char in line:
        if char in 'qwertyuiopasdfghjklzxcvbnm ':
            clean_line += char
        else:
            clean_line += ' '

    clean_line = re.sub(' +', ' ', clean_line)  # delete extra spaces
    print(clean_line)
    if clean_line[0] == ' ':
        clean_line = clean_line[1:]
    return clean_line


def merge_data_in_a_folder(data_dir, output_file_name):
    all_type_file_name = os.listdir(data_dir)
    output_file = os.path.join(data_dir, output_file_name)
    writer = open(output_file, "w")
    for one_type_file_name in all_type_file_name:
        if ("all" in one_type_file_name):
            continue
        cur_file = os.path.join(data_dir, one_type_file_name)
        test_ec = open(cur_file, 'rb')
        ec = chardet.detect(test_ec.read())['encoding']
        with open(cur_file, "r", encoding=ec) as reader:
            writer.write(reader.read())
    writer.close()


def get_vocab_size(filename):
    lines = open(filename, 'r').readlines()

    vocab = set()
    for line in lines:
        words = line[:-1].split(' ')
        for word in words:
            if word not in vocab:
                vocab.add(word)
    print("name|len of dic")
    print(filename, " ", len(vocab))


def get_mean_and_std(filename):
    lines = open(filename, 'r').readlines()

    line_lengths = []
    for line in lines:
        length = len(line[:-1].split(' ')) - 1
        line_lengths.append(length)

    print("name| mean| std| max")
    print(filename, " ", statistics.mean(line_lengths), " ",  statistics.stdev(line_lengths), " ", max(line_lengths))


# =====================
# =====================
# =====gen dict========
# =====================
# =====================



# files: all the statistic; output_pickle_path: output files name; huge_word2vec:Glove Embedding
def gen_vocab_dicts(files, output_pickle_path, huge_word2vec):

    vocab = set()
    text_embeddings = open(huge_word2vec, 'r').readlines()
    word2vec = {}


    #loop through each text file
    for txt_path in files:
    	# get all the words
    	try:
    		all_lines = open(txt_path, "r").readlines()
    		for line in all_lines:
    			words = line[:-1].split(' ')
    			for word in words:
    			    vocab.add(word)
    	except:
    		print(txt_path, "has an error")
    
    print(len(vocab), "unique words found")

    # load the word embeddings, and only add the word to the dictionary if we need it
    for line in text_embeddings:
        items = line.split(' ')
        word = items[0]
        if word in vocab:
            vec = items[1:]
            word2vec[word] = np.asarray(vec, dtype = 'float32')
    print(len(word2vec), "matches between unique words and word2vec dictionary")
        
    pickle.dump(word2vec, open(output_pickle_path, 'wb'))
    print("dictionaries outputted to", output_pickle_path)
