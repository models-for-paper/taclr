from utils import *
import pandas as pd


def getLabelDict(train_data, test_data):
    label_dict = {}
    li = 0
    for i in train_data:
        lis = i.split(":")[0]
        if (lis not in label_dict):
            label_dict[lis] = li
            li = li + 1
    for i in test_data:
        lis = i.split(":")[0]
        if (lis not in label_dict):
            label_dict[lis] = li
            li = li + 1
    return label_dict


if __name__ == "__main__":
    train_path = "../data/trec//train_5500.label"
    test_path = "../data/trec/TREC_10.label"
    
    train_data = open(train_path, 'r', encoding="ISO-8859-1").readlines()
    test_data = open(test_path, 'r', encoding="ascii").readlines()

    label_dict = getLabelDict(train_data, test_data)
    print(label_dict)


    # print(len(trec_lines), len(obj_lines))    
    train_lines = []
    test_lines = []



    # training set
    for line_str in train_data:
        line = line_str.split(":")
        clean_line = str(label_dict[line[0]]) + '\t' + get_only_chars(line[1])
        train_lines.append(clean_line)


    # testing set
    for line_str in test_data:
        line = line_str.split(":")
        clean_line = str(label_dict[line[0]]) + '\t' + get_only_chars(line[1])
        test_lines.append(clean_line)

    print(len(test_lines), len(train_lines))

    # print training set
    writer = open('../data/trec/train_orig.txt', 'w')
    for line in train_lines:
        writer.write(line + '\n')
    writer.close()

    # print testing set
    writer = open('../data/trec/test.txt', 'w')
    for line in test_lines:
        writer.write(line + '\n')
    writer.close()

    gen_vocab_dicts(['../data/trec/train_orig.txt', '../data/trec/test.txt'], '../data/trec/train_trec_word2vec.pkl', '../../../dataset/word2vec/glove.840B.300d.txt')
