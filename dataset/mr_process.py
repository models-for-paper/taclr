from utils import *
import pandas as pd
if __name__ == "__main__":
    train_path = "../data/mr//train.csv"
    test_path = "../data/mr/test.csv"
    train_data = pd.read_csv(train_path, header=None, sep="\t", names=["question", "flag"], quoting=3).fillna("WASHINGTON")
    test_data = data = pd.read_csv(test_path, header=None, sep="\t", names=["question", "flag"], quoting=3).fillna("WASHINGTON")

    # print(len(mr_lines), len(obj_lines))    
    train_lines = []
    test_lines = []

    # training set
    for line in train_data.values:
        clean_line = str(line[1]) + '\t' + get_only_chars(line[0])
        train_lines.append(clean_line)


    # testing set
    for line in test_data.values:
        clean_line = str(line[1]) + '\t' + get_only_chars(line[0])
        test_lines.append(clean_line)

    print(len(test_lines), len(train_lines))

    # print training set
    writer = open('../data/mr/train_orig.txt', 'w')
    for line in train_lines:
        writer.write(line + '\n')
    writer.close()

    # print testing set
    writer = open('../data/mr/test.txt', 'w')
    for line in test_lines:
        writer.write(line + '\n')
    writer.close()

    gen_vocab_dicts(['../data/mr/train_orig.txt', '../data/mr/test.txt'], '../data/mr/train_mr_word2vec.pkl', '../../../dataset/word2vec/glove.840B.300d.txt')
