import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np


class MixCoLoss(nn.Module):
    def __init__(self, args):
        super(MixCoLoss, self).__init__()

        self.batch_size = args.batch_size
        self.device = args.device
        self.temperature = args.temperature

        self.mask_samples_from_same_repr = self._get_correlated_mask().to(self.device).bool()

        self.similarity_function = self._get_similarity_function(
            args.use_cosine_similarity)

        self.criterion = nn.CrossEntropyLoss(reduction='none')

    def _get_similarity_function(self, use_cosine_similarity):
        if use_cosine_similarity:
            return self._cosine_simililarity
        else:
            return self._dot_simililarity

    def _dot_simililarity(self, x, y):
        v = torch.tensordot(x.unsqueeze(1), y.T.unsqueeze(0), dims=2)
        # x shape: (N, 1, C)
        # y shape: (1, C, 2N)
        # v shape: (N, 2N)
        return v

    def _cosine_simililarity(self, x, y):
        # x shape: (N, 1, C)
        # y shape: (1, 2N, C)
        # v shape: (N, 2N)
        v = torch.nn.functional.cosine_similarity(
            x.unsqueeze(1), y.unsqueeze(0), dim=-1)
        return v

    def _get_correlated_mask(self):
        diag = np.eye(self.batch_size)
        mask = torch.from_numpy(diag)
        mask = (1 - mask)
        return mask

    def forward_(self, z1, z2, mix_lambda):
        if (z1.shape[0] != self.batch_size):
            self.batch_size = z1.shape[0]
            self.mask_samples_from_same_repr = self._get_correlated_mask().to(self.device).bool()

        similarity_matrix = self.similarity_function(z1, z2)
        positives = torch.diag(similarity_matrix).view(self.batch_size, 1)

        negatives = similarity_matrix[self.mask_samples_from_same_repr].view(
            self.batch_size, -1)

        logits = torch.cat((positives, negatives), dim=1)
        logits /= self.temperature

        labels = torch.zeros(self.batch_size).to(self.device).long()
        loss = self.criterion(logits, labels)
        return loss.mul(mix_lambda).mean()

    def forward(self, z, mix_z, mixed_z, mix_lambda):
        return self.forward_(z, mixed_z, mix_lambda) + self.forward_(mix_z, mixed_z, (1.0 - mix_lambda))




