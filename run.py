from numpy.lib.function_base import delete
from models.textcnn import *
from models.textrnn import *
from models.textBert import *
from config import *
import pickle
import numpy as np
import torch
import os
from data_process import *
from torch.utils.data import DataLoader, TensorDataset
from all_dataset import au_dataset, au_dataset_bert
from daclr import DaCLR
from torch.optim import Adam
from torch.utils.tensorboard import SummaryWriter
from models.MLP_CLS import clsModel
import torch.nn as nn
import transformers as trm


# use_dp = True


# import argparse
# parser = argparse.ArgumentParser()
# parser.add_argument("--model", required=True, type=str, help="train test eval")
# cli_args = parser.parse_args()

if torch.cuda.is_available():
    device = 'cuda:2'
else:
    device = 'cpu'
#device = 'cpu'
device = torch.device(device)

semi_rate=0.1


writer = SummaryWriter('log/train/ispre/bert/cr')
# writer = SummaryWriter('log/train/rnn/')
# writer = SummaryWriter('log/train/bert/')
# writer = SummaryWriter('log/train/all/')
# writer = SummaryWriter('log/train/test/')
# writer = SummaryWriter('log/train/base/')


class nnCfg():
    def __init__(self, word2vec_len, class_num, sentence_length, word2vec_weight, projection_out_dim, pretrained_path='/home/rjx/dataset/models/bert-base-uncased', pretrain=True):
        self.embed_dim = word2vec_len
        self.class_num = class_num
        self.sentence_length = sentence_length
        self.word2vec_weight = torch.tensor(word2vec_weight).float().to(device)
        self.projection_out_dim = projection_out_dim
        self.pretrained_path = pretrained_path
        self.pretrain = pretrain


class daclrCfg():
    def __init__(self, base_model, batch_size, device, projection_out_dim, temperature=0.5, use_cosine_similarity=True, mixco_loss_weight_gamma=0.5, mix_lambda_alpha=0.2):
        self.base_model = base_model
        self.batch_size = batch_size
        self.device = device
        self.temperature = temperature
        self.use_cosine_similarity = use_cosine_similarity
        self.projection_out_dim = projection_out_dim
        self.mixco_loss_weight_gamma = mixco_loss_weight_gamma
        self.mix_lambda_alpha = mix_lambda_alpha


class clsmodelCfg():
    def __init__(self, projection_out_dim, class_num):
        self.projection_out_dim = projection_out_dim
        self.class_num = class_num


class datasetCfg():
    def __init__(self, train_txt, word2id, id2word, device, input_size, num_classes, alpha_sr, alpha_ri, alpha_rs, alpha_rd, num_aug=3, percent_dataset=1.0, pretrained_path='/home/rjx/dataset/models/bert-base-uncased'):
        self.train_txt = train_txt
        self.word2id = word2id
        self.id2word = id2word

        self.device = device
        self.input_size = input_size
        self.num_classes = num_classes

        self.alpha_sr = alpha_sr
        self.alpha_ri = alpha_ri
        self.alpha_rs = alpha_rs
        self.alpha_rd = alpha_rd
        self.num_aug = num_aug
        self.percent_dataset = percent_dataset

        self.pretrained_path = pretrained_path


def runModel(C, supervise_ratio, pretrain, use_dataset_percent):
    if (C.use_pretrain_clr or C.use_pretrain_cls):
        load_name = C.model_path
        print("@@@ Loading a mode: {} @@@".format(load_name))
        checkpoint = torch.load(load_name)

    test_or_train = "train"
    if (C.use_pretrain_clr):
        test_or_train = "test"
    if (C.eval_switch):
        test_or_train = "eval"

    word2id, id2word, word2vec_weight = get_dict_and_weight(
        C.word2vec_weight_path, C.word2vec_len)

    if (C.eval_switch):
        dataset_args = datasetCfg(C.test_txt, word2id, id2word, device, C.input_size_list[C.set_id], C.num_classes_list[
                                  C.set_id], C.alphas[0], C.alphas[1], C.alphas[2], C.alphas[3], C.num_aug, use_dataset_percent, C.pretrained_path)
    else:
        dataset_args = datasetCfg(C.train_txt, word2id, id2word, device, C.input_size_list[C.set_id], C.num_classes_list[
                                  C.set_id], C.alphas[0], C.alphas[1], C.alphas[2], C.alphas[3], C.num_aug, use_dataset_percent, C.pretrained_path)

    if (C.model_types[C.model_type_id] == 'bert'):
        dt = au_dataset_bert(dataset_args)
    else:
        dt = au_dataset(dataset_args)

    dl = DataLoader(dt, batch_size=C.BATCH_SIZE, shuffle=True, drop_last=False)

    nn_args = nnCfg(300, C.num_classes_list[C.set_id], C.input_size_list[C.set_id],
                    word2vec_weight, C.projection_out_dim, C.pretrained_path, pretrain)

    if (C.model_types[C.model_type_id] == 'rnn'):
        base_model = textRNN(nn_args)
    elif (C.model_types[C.model_type_id] == 'cnn'):
        base_model = textCNN(nn_args)
    else:
        base_model = textBert(nn_args)
    base_model.to(device)
    # if (use_dp):
    #     base_model = nn.DataParallel(base_model)


    daclr_args = daclrCfg(base_model, C.BATCH_SIZE, device, C.projection_out_dim, C.temperature,
                          C.use_cosine_similarity, C.mixco_loss_weight_gamma, C.mix_lambda_alpha)
    model = DaCLR(daclr_args)
    if (C.use_pretrain_clr):
        model.load_state_dict(checkpoint['model'])
    model.to(device)
    # if (use_dp):
    #     model = nn.DataParallel(model)


    if (C.supervise_ratio > 0):
        clsModel_args = clsmodelCfg(
            C.projection_out_dim, C.num_classes_list[C.set_id])
        cls_model = clsModel(clsModel_args)
        if (C.use_pretrain_cls):
            cls_model.load_state_dict(checkpoint['cls_model'])
        cls_model.to(device)    
        # if (use_dp):
        #     cls_model = nn.DataParallel(cls_model)
    

    print("@@@@ Build a {} Model with: {} paramenters @@@@".format(
        C.model_types[C.model_type_id], sum(param.numel() for param in model.parameters())))
    # for name,parameters in model.named_parameters():
    #     print(name,':',parameters.size())

    if (C.supervise_ratio > 0):
        optimizer = Adam([
            {'params': model.parameters(), 'lr': C.LR},
            {'params': cls_model.parameters(), 'lr': C.LR}
        ], weight_decay=C.WEIGHT_DECAY)
    else:
        optimizer = Adam(model.parameters(), lr=C.LR,
                         weight_decay=C.WEIGHT_DECAY)

    if (C.eval_switch):
        model.eval()
        if (C.supervise_ratio > 0):
            cls_model.eval()
    else:
        model.train()
        if (C.supervise_ratio > 0):
            cls_model.train()

    B = len(dl)
    if (C.eval_switch):  # eval supervise
        sum_total = 0
        sum_correct = 0
        for e in range(1):
            for i, data in enumerate(dl):
                _clr_loss = model(data[1], data[2])
                if (C.supervise_ratio > 0):
                    h, _ = base_model(data[0]) # use the origin text data.
                    _cls_loss, _cls_logist = cls_model(h, data[3]) # hidden and label
                    _, predicted = torch.max(_cls_logist.data, 1)
                    sum_total += data[3].shape[0]
                    sum_correct += predicted.eq(
                        data[3].data).cpu().long().sum().item()
                    loss = (1.0-C.supervise_ratio)*_clr_loss + \
                        C.supervise_ratio*_cls_loss
                else:
                    raise Exception('C.supervise_ratio:{}, is not 1.0!'.format(C.supervise_ratio))

                writer.add_scalar('Loss-{}-{}-{}-{}'.format(
                    C.datasets[C.set_id], C.model_types[C.model_type_id], C.train_test_eval_switch, supervise_ratio), loss.item(), global_step=B*e+i)

                if i % int(0.2*B) == 0:
                    print("{}/{} epoch | {}/{} batch | Loss: {:.4f} | ACC: {:.4f}".format(e,
                                                                                          C.EPOCH, i, B, loss.item(), sum_correct/sum_total))

        writer.add_text('ACC-{}-{}-{}-{}'.format(C.datasets[C.set_id], C.model_types[C.model_type_id], C.train_test_eval_switch, supervise_ratio),
                        "total: {} | correct: {} | acc: {:.6f}".format(sum_total, sum_correct, sum_correct/sum_total), global_step=1)

        print("The ACC of the test dataset: {:.6f}".format(
            sum_correct/sum_total))
    else:  # train (train the base_model or base_mode + cls_mode) or test (train the cls_model)
        for e in range(C.EPOCH):
            for i, data in enumerate(dl):
                optimizer.zero_grad()
                _clr_loss = model(data[1], data[2])
                if (C.supervise_ratio > 0):
                    if (C.test_only_EDA):
                        h, _ = base_model(data[1]) # use the augmented text data.
                    else:
                        h, _ = base_model(data[0]) # use the origin text data.
                    _cls_loss, _ = cls_model(h, data[3])
                    if C.train_test_eval_switch == 'train' and semi_rate > 0:
                        if (np.random.rand()<semi_rate):
                            _clr_loss = 0.0
                    loss = (1.0-C.supervise_ratio)*_clr_loss + \
                        C.supervise_ratio*_cls_loss
                else:
                    loss = _clr_loss
                    
                loss.backward()
                optimizer.step()

                writer.add_scalar('Loss-{}-{}-{}-{}'.format(
                    C.datasets[C.set_id], C.model_types[C.model_type_id], C.train_test_eval_switch, supervise_ratio), loss.item(), global_step=B*e+i)

                if i % int(0.2*B) == 0:
                    print("{}/{} epoch | {}/{} batch | Loss: {:.4f}".format(e,
                                                                            C.EPOCH, i, B, loss.item()))
                # torch.Size([32, 50]) torch.Size([32, 50]) torch.Size([32, 2]) for data[0].shape, data[1].shape, data[2].shape
            if (e == C.EPOCH-1 or e % int(0.2*C.EPOCH) == 0):
                if (C.supervise_ratio > 0):
                    state = {'model': model.state_dict(
                    ), 'cls_model': cls_model.state_dict()}
                    torch.save(state, os.path.join(C.model_checkpoints_folder, '{}/CLR_CLS{}_{}_e{}_{}_{}.dit'.format(
                        C.datasets[C.set_id], supervise_ratio, C.model_types[C.model_type_id], e, test_or_train, use_dataset_percent)))
                else:
                    state = {'model': model.state_dict()}
                    torch.save(state, os.path.join(C.model_checkpoints_folder, '{}/CLR_{}_e{}_{}_{}.dit'.format(
                        C.datasets[C.set_id], C.model_types[C.model_type_id], e, test_or_train, use_dataset_percent)))

    # force release gpu resource
    base_model.to(torch.device('cpu'))
    model.to(torch.device('cpu'))
    del base_model
    del model
    del optimizer
    del dl
    del C


if __name__ == "__main__":
    BATCH_SIZE = 1024
    EPOCH = 100
    LR = 1e-3
    WEIGHT_DECAY = 10e-5
    train_and_test_epoch = EPOCH-1  # test epoch
    supervise_ratios = [0.0, 0.5, 1.0] # 0 for unspervise train (TDACLR), 0.5 for sim-supervise (supervise TDACLR), 1.0 for full-supervise (i.e., EDA)

    model_type_ids = [0, 1, 2]  # cnn 0, rnn 1, bert 2
    set_ids = [0, 2, 1, 3, 4]  # cr 0, sst 1, subj 2, trec 3, mr 4
    train_test_eval_index_ids = [0, 1, 2]  # train 0, test 1, eval 2
    use_dataset_percents = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]


    single_test = True
    test_only_EDA = False
    pretrain = True
    # semi_rate = 0.0

    if (single_test):
        for model_type_id in [0]:
            print("###model_type_id  {} ###".format(model_type_id))
            for set_id in [2]:
                print("###set_id  {} ###".format(set_id))
                for supervise_ratio in [0.0]:
                    print("###supervise_ratio  {} ###".format(supervise_ratio))
                    for train_test_eval_index_id in train_test_eval_index_ids:
                        print("###train_test_eval_index_id  {} ###".format(train_test_eval_index_id))
                        for use_dataset_percent in [1.0]:
                        # for use_dataset_percent in use_dataset_percents:
                            print("###use_dataset_percent  {} ###".format(use_dataset_percent))
                            C = genTrainCfg(BATCH_SIZE, EPOCH, LR, WEIGHT_DECAY, model_type_id, set_id,
                                            train_test_eval_index_id, train_and_test_epoch, supervise_ratio, test_only_EDA, use_dataset_percent)
                            runModel(C, supervise_ratio, pretrain, use_dataset_percent)
    else:
        for model_type_id in model_type_ids:
            print("###model_type_id  {} ###".format(model_type_id))
            for set_id in set_ids:
                print("###set_id  {} ###".format(set_id))
                for supervise_ratio in supervise_ratios:
                    print("###supervise_ratio  {} ###".format(supervise_ratio))
                    for train_test_eval_index_id in train_test_eval_index_ids:
                        print("###train_test_eval_index_id  {} ###".format(train_test_eval_index_id))
                        for use_dataset_percent in use_dataset_percents:
                            print("###use_dataset_percent  {} ###".format(use_dataset_percent))
                            C = genTrainCfg(BATCH_SIZE, EPOCH, LR, WEIGHT_DECAY, model_type_id, set_id,
                                            train_test_eval_index_id, train_and_test_epoch, supervise_ratio, test_only_EDA, use_dataset_percent)
                            runModel(C, supervise_ratio, pretrain, use_dataset_percent)
    writer.close() # close the tensorboard logging






