import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np


class NTXentLoss(nn.Module):
    def __init__(self, args):
        super(NTXentLoss, self).__init__()
        self.args = args

        self.batch_size = args.batch_size
        self.device = args.device
        self.temperature = args.temperature

        self.mask_samples_from_same_repr = self._get_correlated_mask().to(self.device).bool()

        self.similarity_function = self._get_similarity_function(
            args.use_cosine_similarity)

        self.criterion = nn.CrossEntropyLoss()

    def _get_similarity_function(self, use_cosine_similarity):
        if use_cosine_similarity:
            return self._cosine_simililarity
        else:
            return self._dot_simililarity

    def _dot_simililarity(self, x, y):
        v = torch.tensordot(x.unsqueeze(1), y.T.unsqueeze(0), dims=2)
        # x shape: (N, 1, C)
        # y shape: (1, C, 2N)
        # v shape: (N, 2N)
        return v

    def _cosine_simililarity(self, x, y):
        # x shape: (N, 1, C)
        # y shape: (1, 2N, C)
        # v shape: (N, 2N)
        v = torch.nn.functional.cosine_similarity(
            x.unsqueeze(1), y.unsqueeze(0), dim=-1)
        return v

    def _get_correlated_mask(self):
        diag = np.eye(2 * self.batch_size)
        l1 = np.eye((2 * self.batch_size), 2 *
                    self.batch_size, k=-self.batch_size)
        l2 = np.eye((2 * self.batch_size), 2 *
                    self.batch_size, k=self.batch_size)
        mask = torch.from_numpy((diag + l1 + l2))
        mask = (1 - mask)
        return mask

    def forward(self, z1, z2):
        # update the batch size and the negive mask
        if (z1.shape[0] != self.batch_size):
            self.batch_size = z1.shape[0]
            self.mask_samples_from_same_repr = self._get_correlated_mask().to(self.device).bool()

        representations = torch.cat([z1, z2], dim=0)
        similarity_matrix = self.similarity_function(
            representations, representations)
        l_pos = torch.diag(similarity_matrix, self.batch_size)
        r_pos = torch.diag(similarity_matrix, -self.batch_size)
        positives = torch.cat([l_pos, r_pos]).view(2 * self.batch_size, 1)
        negatives = similarity_matrix[self.mask_samples_from_same_repr].view(
            2 * self.batch_size, -1)

        logits = torch.cat((positives, negatives), dim=1)
        logits /= self.temperature

        labels = torch.zeros(2 * self.batch_size).to(self.device).long()
        loss = self.criterion(logits, labels)
        return loss






